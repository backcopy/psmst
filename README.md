# Partially Signed Multi Signature Transaction

**Abstract:** As an _optional guideline_, we present an interoperable standard for exchanging multi signature transactions that are partially signed. 




# Overview
```mermaid
graph TD
A[User 1 generate public key]
B[User 2 generate public key]
C[User 3 generate public key]

A --> D
B --> D
C --> D

D[Negotiate number of required<br/>signatures to execute a spend]

D --> E

E[Exchange all public keys between parties]

E --> F

F[Derive common multi signature address]

F --> G

G[Fund multi signature address]

G --> H

H[User 1 signs PSMST]

H --> I
I[User 1 Review PSMST] 

I --> J
J[User 2 signs PSMST] 

J --> K
K[User 2 submits to network]
```


## Specification

Coming soon.

## Software

Coming soon.

## Versioning

Coming soon.

## Contributing

Coming soon.

## License

Coming soon.

## Acknowledgments

Coming soon.
